﻿using System;

namespace DotNetCore.Learning
{
    class Program
    {
        static void Main(string[] args)
        {
            //// Lees het csv bestand in en toon ingelezen string
            //Console.WriteLine(Dal.TryOut.ReadPostcodesFromCSVFile());
            //// zet stringvoorstelling postcodes om in een lijst
            //// van postcode-objecten en toon de lijst in de console
            //Dal.TryOut.ListPostcodes(Dal.TryOut.GetPostcodeList());
            //Console.WriteLine(Dal.TryOut.SerializeObjectToCsv(Dal.TryOut.GetPostcodeList(), ";"));
            //Dal.TryOut.SerializeCsvToXml();
            //Dal.TryOut.ListPostcodes(Dal.TryOut.GetPostcodeListFromXml());
            //Dal.TryOut.SerializeCsvToJson();
            //Dal.TryOut.ListPostcodes(Dal.TryOut.GetPostcodeListFromJson());
            ////Console.ReadKey();

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Generieke lijst van XML");
            Console.ForegroundColor = ConsoleColor.White;
            BookSerializing.ShowBooks(BookSerializing.DeserializeFromXml());
            BookSerializing.SerializeListToCsv(BookSerializing.DeserializeFromXml(), "|");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Generieke lijst van CSV");
            Console.ForegroundColor = ConsoleColor.White;
            BookSerializing.ShowBooks(BookSerializing.DeserializeCsvToList());
            BookSerializing.SerializeListToJson(BookSerializing.DeserializeCsvToList());
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Generieke lijst van JSON");
            Console.ForegroundColor = ConsoleColor.White;
            BookSerializing.ShowBooks(BookSerializing.DeserializeJsonToList());
        }
    }
}
