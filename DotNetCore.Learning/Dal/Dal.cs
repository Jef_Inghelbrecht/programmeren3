﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetCore.Learning.Dal
{
    class Dal
    {
        public static Bll.Postcode[] GetPostcodeArray()
        {
            string[] lines = Postcode.LeesUitCsvBestand().Split('\n');
            Bll.Postcode[] postcodes = new Bll.Postcode[lines.Length];
            int i = 0;
            foreach (string s in lines)
            {
                if (s.Length > 0)
                {
                    postcodes[i++] = Postcode.ToObject(s);
                }
            }
            return postcodes;
        }

        public static Bll.Book[] GetBookArray(List<Bll.Book>boekenLijst)
        {
            Bll.Book[] boeken = boekenLijst.ToArray();
            return boeken;
        }
    }
}
