﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace DotNetCore.Learning.Dal
{
    class TryOut
    {
        public static string ReadPostcodesFromCSVFile()
        {
            Helpers.Tekstbestand bestand = new Helpers.Tekstbestand();
            bestand.FileName = @"../../../Data/Postcodes.csv";
            bestand.Lees();
            return bestand.Text;
        }

        public static Bll.Postcode PostcodeCsvToObject(string line)
        {
            Bll.Postcode postcode = new Bll.Postcode();
            string[] values = line.Split('|');
            postcode.Code = values[0];
            postcode.Plaats = values[1];
            postcode.Provincie = values[2];
            postcode.Localite = values[3];
            postcode.Province = values[4];
            return postcode;
        }

        public static List<Bll.Postcode> GetPostcodeList()
        {

            string[] postcodes = ReadPostcodesFromCSVFile().Split('\n');
            List<Bll.Postcode> list = new List<Bll.Postcode>();
            foreach (string s in postcodes)
            {
                if (s.Length > 0)
                {
                    list.Add(PostcodeCsvToObject(s));
                }
            }
            return list;
        }

        public static string SerializeObjectToCsv(List<Bll.Postcode> list, string separator)
        {
            string fileName = @"../../../Data/Postcodes2.csv";
            string message;
            try
            {
                using TextWriter writer = new StreamWriter(fileName);
                foreach (Bll.Postcode item in list)
                {
                    // One of the most versatile and useful additions to the C# language in version 6
                    // is the null conditional operator ?.           
                    writer.WriteLine("{0}{5}{1}{5}{2}{5}{3}{5}{4}",
                        item.Code,
                        item.Plaats,
                        item.Provincie,
                        item.Localite,
                        item.Province,
                        separator);
                }
                message = $"Het bestand met de naam {fileName} is gemaakt!";
            }
            catch (Exception e)
            {
                // Melding aan de gebruiker dat iets verkeerd gelopen is.
                // We gebruiken hier de nieuwe mogelijkheid van C# 6: string interpolatie
                message = $"Kan het bestand met de naam {fileName} niet maken.\nFoutmelding {e.Message}.";
            }
            return message;
        }

        public static void ListPostcodes(List<Bll.Postcode> list)
        {
            foreach (Bll.Postcode postcode in list)
            {
                // One of the most versatile and useful additions to the C# language in version 6
                // is the null conditional operator ?.Post           
                Console.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}",
                    postcode?.Code,
                    postcode?.Plaats,
                    postcode?.Provincie,
                    postcode?.Localite,
                    postcode?.Province);
            }
        }

        public static void SerializeCsvToXml()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Bll.Postcode[]));
            Bll.Postcode postcode = new Bll.Postcode();
            TextWriter writer = new StreamWriter(@"../../../Data/Postcode.xml");
            //De serializer werkt niet voor een generieke lijst en ook niet voor ArrayList
            Bll.Postcode[] postcodes = Dal.GetPostcodeArray();
            serializer.Serialize(writer, postcodes);
            writer.Close();
        }

        public static List<Bll.Postcode> GetPostcodeListFromXml()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Bll.Postcode[]));
            Bll.Postcode postcode = new Bll.Postcode();
            StreamReader file = new System.IO.StreamReader(@"../../../Data/Postcode.xml");
            Bll.Postcode[] postcodes = (Bll.Postcode[])serializer.Deserialize(file);
            file.Close();
            // array converteren naar List
            return new List<Bll.Postcode>(postcodes);
        }

        public static void SerializeCsvToJson()
        {
            Postcode postcode = new Postcode();
            TextWriter writer = new StreamWriter(@"../../../Data/Postcode.json");
            //De serializer werkt niet voor een generieke lijst en ook niet voor ArrayList
            Bll.Postcode[] postcodes = Dal.GetPostcodeArray();
            // static method SerilizeObject van Newtonsoft.Json
            string postcodeString = JsonConvert.SerializeObject(postcodes);
            writer.WriteLine(postcodeString);
            writer.Close();
        }

        public static List<Bll.Postcode> GetPostcodeListFromJson()
        {
            Helpers.Tekstbestand bestand = new Helpers.Tekstbestand();
            bestand.FileName = @"../../../Data/Postcode.json";
            bestand.Lees();
            List<Bll.Postcode> list = JsonConvert.DeserializeObject<List<Bll.Postcode>>(bestand.Text);
            return list;
        }
    }
}
