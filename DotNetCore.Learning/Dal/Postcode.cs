﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetCore.Learning.Dal
{
    class Postcode
    {
        public static string LeesUitCsvBestand()
        {
            Helpers.Tekstbestand bestand = new Helpers.Tekstbestand();
            // je zou voor de bestandsnaam eventueel ook een property kunnen gebruiken
            // zodat die hier niet hard coded staat
            bestand.FileName = @"..\..\..\Data\Postcodes.csv";
            bestand.Lees();
            return bestand.Text;
        }

        public static List<Bll.Postcode> GetList()
        {

            string[] postcodes = LeesUitCsvBestand().Split('\n');
            List<Bll.Postcode> list = new List<Bll.Postcode>();
            foreach (string s in postcodes)
            {
                if (s.Length > 0)
                {
                    list.Add(ToObject(s));
                }
            }
            return list;
        }

        public static Bll.Postcode ToObject(string line)
        {
            Bll.Postcode postcode = new Bll.Postcode();
            string[] values = line.Split('|');
            postcode.Code = values[0];
            postcode.Plaats = values[1];
            postcode.Provincie = values[2];
            postcode.Localite = values[3];
            postcode.Province = values[4];
            return postcode;
        }
    }
}
