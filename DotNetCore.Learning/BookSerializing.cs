﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace DotNetCore.Learning
{
    class BookSerializing
    {
        public static List<Bll.Book> DeserializeFromXml()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Bll.Book[]));
            Bll.Book postcode = new Bll.Book();
            StreamReader file = new System.IO.StreamReader(@"../../../Data/Book.xml");
            Bll.Book[] bookList = (Bll.Book[])serializer.Deserialize(file);
            file.Close();
            // array converteren naar List
            return new List<Bll.Book>(bookList);
        }

        public static string SerializeListToCsv(List<Bll.Book> list, string separator)
        {
            string fileName = @"../../../Data/BookList.csv";
            string message;
            try
            {
                using TextWriter writer = new StreamWriter(fileName);
                foreach (Bll.Book item in list)
                {
                    // One of the most versatile and useful additions to the C# language in version 6
                    // is the null conditional operator ?.           
                    writer.WriteLine("{0}{8}{1}{8}{2}{8}{3}{8}{4}{8}{5}{8}{6}{8}{7}",
                        item.Title,
                        item.Year,
                        item.City,
                        item.Publisher,
                        item.Author,
                        item.Edition,
                        item.Translator,
                        item.Comment,
                        separator);
                }
                message = $"Het bestand met de naam {fileName} is gemaakt!";
            }
            catch (Exception e)
            {
                // Melding aan de gebruiker dat iets verkeerd gelopen is.
                // We gebruiken hier de nieuwe mogelijkheid van C# 6: string interpolatie
                message = $"Kan het bestand met de naam {fileName} niet maken.\nFoutmelding {e.Message}.";
            }
            return message;
        }

        public static List<Bll.Book> DeserializeCsvToList()
        {
            Helpers.Tekstbestand bestand = new Helpers.Tekstbestand();
            bestand.FileName = @"../../../Data/BookList.csv";
            bestand.Lees();

            string[] boekenLijst = bestand.Text.Split('\n');
            List<Bll.Book> list = new List<Bll.Book>();
            foreach (string s in boekenLijst)
            {
                if (s.Length > 0)
                {
                    Bll.Book boek = new Bll.Book();
                    string[] boekWaardes = s.Split('|');
                    boek.Title = boekWaardes[0];
                    boek.Year = boekWaardes[1];
                    boek.City = boekWaardes[2];
                    boek.Publisher = boekWaardes[3];
                    boek.Author = boekWaardes[4];
                    boek.Edition = boekWaardes[5];
                    boek.Translator = boekWaardes[6];
                    boek.Comment = boekWaardes[7];
                    list.Add(boek);
                }
            }
            return list;
        }

        public static void SerializeListToJson(List<Bll.Book> books)
        {
            TextWriter writer = new StreamWriter(@"../../../Data/BookList.json");
            //De serializer werkt niet voor een generieke lijst en ook niet voor ArrayList
            Bll.Book[] boeken = Dal.Dal.GetBookArray(books);
            // static method SerilizeObject van Newtonsoft.Json
            string boekString = JsonConvert.SerializeObject(boeken);
            writer.WriteLine(boekString);
            writer.Close();
        }

        public static List<Bll.Book> DeserializeJsonToList()
        {
            Helpers.Tekstbestand bestand = new Helpers.Tekstbestand();
            bestand.FileName = @"../../../Data/BookList.json";
            bestand.Lees();
            List<Bll.Book> list = JsonConvert.DeserializeObject<List<Bll.Book>>(bestand.Text);
            return list;
        }

        public static void ShowBooks(List<Bll.Book>boekenLijst)
        {
            foreach (Bll.Book item in boekenLijst)
            {
                Console.Write($"{item.Title}|{item.Year}|{item.City}|{item.Publisher}|{item.Author}|{item.Edition}|{item.Translator}|{item.Comment}\n");
            }
        }
    }
}
