﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace DotNetCore.Learning
{
    class BookSerializing
    {
        //Deserialiseren van XML bestand naar generieke lijst
        public static List<Book> DeserializeFromXml()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Book[]));
            Book book = new Book();
            StreamReader file = new System.IO.StreamReader(@"Data\Book.xml");
            Book[] books= (Book[])serializer.Deserialize(file);
            file.Close();
            Console.WriteLine($"XML is met succes gedeserialiseerd.");
            // array converteren naar List
            return new List<Book>(books);

        }

        //Generieke lijst serialiseren naar CSV bestand
        public static string SerializeListToCsv(List<Book> books, string separator)
        {
            string fileName = @"Data\Book.csv";
            string message;
            try
            {
                TextWriter writer = new StreamWriter(fileName);
                foreach (Book item in books)
                {
                    // One of the most versatile and useful additions to the C# language in version 6
                    // is the null conditional operator ?.           
                    writer.WriteLine("{0}{8}{1}{8}{2}{8}{3}{8}{4}{8}{5}{8}{6}{8}{7}",
                        item?.Title,
                        item?.Year,
                        item?.City,
                        item?.Publisher,
                        item?.Author,
                        item?.Edition,
                        item?.Translator,
                        item?.Comment,
                        separator);
                }
                message = $"Het bestand met de naam {fileName} is gemaakt!";
            }
            catch (Exception e)
            {
                // Melding aan de gebruiker dat iets verkeerd gelopen is.
                // We gebruiken hier de nieuwe mogelijkheid van C# 6: string interpolatie
                message = $"Kan het bestand met de naam {fileName} niet maken.\nFoutmelding {e.Message}.";
            }
            return message;
        }

        //CSV omzetten naar een list

        public static List<Book> DeserializeCsvToList()
        {
            using (StreamReader sr = new StreamReader(@"Data\Book.csv"))
            {
                string text = sr.ReadToEnd();
                string[] boeken = text.Split('\n');

                List <Book> list = new List<Book>();
                foreach (string b in boeken)
                {
                    if (b.Length > 0)
                    {
                        list.Add(BookCsvToObject(b));
                    }
                }
                Console.WriteLine($"CSV is met succes gedeserialiseerd.");
                return list;

            }
        }
        //CSV deserialiseren


        public static Book BookCsvToObject(string line)
        {
            Book book = new Book();
            string[] values = line.Split('|');
            book.Title = values[0];
            book.Year = values[1];//foute opmaak van csv bestand ? error 
            book.City = values[2];
            book.Publisher = values[3];
            book.Author = values[4];
            book.Edition = values[5];
            book.Translator = values[6];
            book.Comment = values[7];
            return book;
        }

        //Omzetten van CSV naar JSON
        public static string SerializeListToJson(List<Book> bookList)
        {

            
            Book book = new Book();
            TextWriter writer = new StreamWriter(@"Data/Book.json");
            //De serializer werkt niet voor een generieke lijst en ook niet voor ArrayList
            //List<Book> bookList = DeserializeFromXml();
            // static method SerilizeObject van Newtonsoft.Json
            string bookString = Newtonsoft.Json.JsonConvert.SerializeObject(bookList);
            writer.WriteLine(bookString);
            writer.Close();
            return bookString;
        }

        //Omzetten JSOn naar list

        public static List<Book> DeserializeJsonToList(List<Book> bookList)
        {
            //List<Book> bookList = DeserializeFromXml();
            List<Book> list = JsonConvert.DeserializeObject<List<Book>>(Newtonsoft.Json.JsonConvert.SerializeObject(bookList));
            return list;
        }

        //Toon resultaat in generieke lijst

        public static void ShowBooks(List<Book> list)
        {
            foreach (Book book in list)
            {
                // One of the most versatile and useful additions to the C# language in version 6
                // is the null conditional operator ?.Post           
                Console.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}",
                    book?.Title,
                    book?.Year,
                    book?.City,
                    book?.Publisher,
                    book?.Author,
                    book?.Edition,
                    book?.Translator,
                    book?.Comment);
            }
        }
    }
}
