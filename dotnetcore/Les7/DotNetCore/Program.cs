﻿using System;

namespace DotNetCore
{
    class Program
    {
        static void Main(string[] args)
        {

            //ik heb geen gebruik gemaakt van het 'leestbestand' zoals in voorbeelden aangezien hier niet van gesproken werd in de opgave.
            //Ik ben steeds verdergegaan met de data die resulteerde in een vorige opgave.

            //Oefening 1
            //DotNetCore.Learning.BookSerializing.DeserializeFromXml();
            //Console.ReadKey();


            //Oefening 2 Centraliseren naar csv bestand
            //Console.WriteLine(DotNetCore.Learning.BookSerializing.SerializeListToCsv(DotNetCore.Learning.BookSerializing.DeserializeFromXml(), "|"));
            //Console.ReadKey();

            //Oefening 3
            DotNetCore.Learning.BookSerializing.DeserializeCsvToList();
            Console.ReadKey();

            //Oefening 4
            DotNetCore.Learning.BookSerializing.SerializeListToJson(DotNetCore.Learning.BookSerializing.DeserializeFromXml());
            Console.ReadKey();

            //Oefening 5
            DotNetCore.Learning.BookSerializing.DeserializeJsonToList(DotNetCore.Learning.BookSerializing.DeserializeFromXml());
            Console.ReadKey();

            //Oefening 6
            DotNetCore.Learning.BookSerializing.ShowBooks(DotNetCore.Learning.BookSerializing.DeserializeJsonToList(DotNetCore.Learning.BookSerializing.DeserializeFromXml()));
            Console.ReadKey();
        }

        
    }
}
