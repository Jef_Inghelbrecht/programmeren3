﻿using System;

namespace Tekstbestand
{
    class Program
    {
        static void Main(string[] args)
        {

            // Lees het csv bestand in en toon ingelezen string
            //Console.WriteLine(DotNetCore.Learning.TryOut.ReadPostcodesFromCSVFile());
            // zet stringvoorstelling postcodes om in een lijst
            // van postcode-objecten en toon de lijst in de console
            //DotNetCore.Learning.TryOut.ListPostcodes(DotNetCore.Learning.TryOut.GetPostcodeList());
            //Console.ReadKey();

            //Console.WriteLine(DotNetCore.Learning.TryOut.SerializeObjectToCsv(DotNetCore.Learning.TryOut.GetPostcodeList(), "|"));

            //Console.ReadKey();


            //DotNetCore.Learning.TryOut.ListPostcodes(DotNetCore.Learning.TryOut.GetPostcodeListFromJson());

            //DotNetCore.Learning.TryOut.SerializeCsvToXml();
            // DotNetCore.Learning.TryOut.ListPostcodes(DotNetCore.Learning.TryOut.GetPostcodeList());

            // DotNetCore.Learning.TryOut.SerializeCsvToJson();
            DotNetCore.Learning.TryOut.GetPostcodeListFromJson();

            Console.ReadKey();
        }
    }
}
