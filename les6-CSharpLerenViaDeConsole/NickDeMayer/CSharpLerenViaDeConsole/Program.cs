﻿using System;

namespace CSharpLerenViaDeConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            //LerenWerkenMetGegevensInCSharp.WerkenMetGegevens.CharLerenGebruiken();
            //LerenWerkenMetGegevensInCSharp.WerkenMetGegevens.ShowAllAsciiValues();
            //LerenWerkenMetGegevensInCSharp.WerkenMetGegevens.StringsZijnOveranderlijk();
            //LerenWerkenMetGegevensInCSharp.WerkenMetGegevens.VerbatimStrings();
            //LerenWerkenMetGegevensInCSharp.WerkenMetGegevens.FormatNumericSample();
            //LerenWerkenMetGegevensInCSharp.WerkenMetGegevens.FormatDate ();
            //Console.WriteLine(LerenWerkenMetGegevensInCSharp.WerkenMetGegevens.CultureInfoo());
            //LerenWerkenMetGegevensInCSharp.WerkenMetGegevens.Interpolatie();
            //Console.WriteLine(LerenWerkenMetGegevensInCSharp.WerkenMetGegevens.TekenreeksSamenvoegen());
            //Console.WriteLine(LerenWerkenMetGegevensInCSharp.WerkenMetGegevens.TekenreeksenVergelijken());
            //LerenWerkenMetGegevensInCSharp.WerkenMetGegevens.CultuurInformatie();
            //LerenWerkenMetGegevensInCSharp.WerkenMetGegevens.VergelijkString();


            //Verschil tussen struct=value en class=reference
            StructVersusClass.TestClass tc = new StructVersusClass.TestClass();
            StructVersusClass.TestStruct ts = new StructVersusClass.TestStruct();

            tc.TestId = 666;
            ts.TestId = 777;

            Console.WriteLine("Class voor het wijzigen:" + tc.TestId);
            Console.WriteLine("Struct voor het wijzigen:" + ts.TestId);

            StructVersusClass.WijzigTestClass(tc);
            StructVersusClass.WijzigTestStruct(ts);

            Console.WriteLine("Class NA het wijzigen:" + tc.TestId);
            Console.WriteLine("Struct NA het wijzigen:" + ts.TestId);

            //Oefening tabel maken
            StructVersusClass.TabelMetGetallen();

            Console.ReadLine();
        }
    }
}
