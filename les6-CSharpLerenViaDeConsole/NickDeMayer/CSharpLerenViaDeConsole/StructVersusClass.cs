﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharpLerenViaDeConsole
{
    class StructVersusClass
    {

        public struct TestStruct
        {
            public int TestId { get; set; }
        }

        public class TestClass
        {
            public int TestId { get; set; }
        }

        public static void WijzigTestStruct (TestStruct input)
        {
            input.TestId = 0;
        }

        public static void WijzigTestClass (TestClass input)
        {
            input.TestId = 0;
        }

        public static void TabelMetGetallen()
        {
            
            for (int i = 0; i < 100; i += 10)
            {
                for (int j = 1; j <= 10; j++)
                {
                    Console.Write(String.Format("{0,-5}", (i+j).ToString()));
                }

                Console.WriteLine ();
                
            }

        
        }   
          

    }
}
