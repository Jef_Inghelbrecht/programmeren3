﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;

namespace LerenWerkenMetGegevensInCSharp
{
    class WerkenMetGegevens
    {
        public static void CharLerenGebruiken()
        {
            char ch2 = '2';
            string str1 = "Upper Case";
            Console.WriteLine(char.GetUnicodeCategory('a'));    // Output: "LowercaseLetter"
            Console.WriteLine(char.GetUnicodeCategory(ch2));    // Output: "DecimalDigitNumber"
            Console.WriteLine(char.GetUnicodeCategory(str1, 6));	// Output: "UppercaseLetter

            char chA = 'A';
            char ch1 = '1';
            string str = "test string";

            Console.WriteLine(chA.CompareTo('B'));      // Output: "-1" (meaning 'A' is 1 less than 'B')
            Console.WriteLine(chA.Equals('A'));     // Output: "True"
            Console.WriteLine(Char.GetNumericValue(ch1));   // Output: "1"
            Console.WriteLine(Char.IsControl('\t'));    // Output: "True"
            Console.WriteLine(Char.IsDigit(ch1));       // Output: "True"
            Console.WriteLine(Char.IsLetter(','));      // Output: "False"
            Console.WriteLine(Char.IsLower('u'));       // Output: "True"
            Console.WriteLine(Char.IsNumber(ch1));      // Output: "True"
            Console.WriteLine(Char.IsPunctuation('.')); // Output: "True"
            Console.WriteLine(Char.IsSeparator(str, 4));    // Output: "True"
            Console.WriteLine(Char.IsSymbol(' '));      // Output: "True"
            Console.WriteLine(Char.IsWhiteSpace(str, 4));   // Output: "True"
            Console.WriteLine(Char.Parse("S"));     // Output: "S"
            Console.WriteLine(Char.ToLower('M'));       // Output: "m"
            Console.WriteLine('x'.ToString());		// Output: "x"
        }

        public static void ShowAllAsciiValues()
        {
            for (int i = 0; i < 256; i++)
            {
                Console.WriteLine((char)i);
            }
        }

        public static void StringsZijnOnveranderlijk()
        {
            String file = "F:/Cursussen/ASP.NET/Ajax/MAFc/Ajax.js";
            if (file.ToUpperInvariant().Substring(10, 12).EndsWith("EXE"))
            {
                Console.WriteLine("Het bestand met de naam {0} is een uitvoerbaar bestand, file");
            }
            else
            {
                Console.WriteLine("Het bestand met de naam {0} is geen uitvoerbaar bestand, file");
            }
        }

        public static void VerbatimStrings()
        {
            string file = "F:\\Cursussen\\ASP.NET\\Ajax\\MAFc\\js";
            Console.Write("Met escape tekens: ");
            Console.WriteLine(file);

            string fileVerbatim = @"F:\Cursussen\ASP.NET\Ajax\MAFc\js";
            Console.Write("Zonder escape tekens maar met een @ ervoor: ");
            Console.WriteLine(fileVerbatim);
        }

        public static string FormatNumericSample()
        {
            double value;

            value = 123;
            Console.WriteLine(value.ToString("00000"));
            Console.WriteLine(String.Format("{0:00000}", value));
            // Displays 00123 

            value = 1.2;
            Console.WriteLine(value.ToString("0.00", CultureInfo.InvariantCulture));
            Console.WriteLine(String.Format(CultureInfo.InvariantCulture,
                        "{0:0.00}", value));
            // Displays 1.20

            Console.WriteLine(value.ToString("00.00", CultureInfo.InvariantCulture));
            Console.WriteLine(String.Format(CultureInfo.InvariantCulture,
                                        "{0:00.00}", value));
            // Displays 01.20

            CultureInfo daDK = CultureInfo.CreateSpecificCulture("da-DK");
            Console.WriteLine(value.ToString("00.00", daDK));
            Console.WriteLine(String.Format(daDK, "{0:00.00}", value));
            // Displays 01,20 

            value = .56;
            Console.WriteLine(value.ToString("0.0", CultureInfo.InvariantCulture));
            Console.WriteLine(String.Format(CultureInfo.InvariantCulture,
                                        "{0:0.0}", value));
            // Displays 0.6 

            value = 1234567890;
            Console.WriteLine(value.ToString("0,0", CultureInfo.InvariantCulture));
            Console.WriteLine(String.Format(CultureInfo.InvariantCulture,
                                        "{0:0,0}", value));
            // Displays 1,234,567,890      

            CultureInfo elGR = CultureInfo.CreateSpecificCulture("el-GR");
            Console.WriteLine(value.ToString("0,0", elGR));
            Console.WriteLine(String.Format(elGR, "{0:0,0}", value));
            // Displays 1.234.567.890 

            value = 1234567890.123456;
            Console.WriteLine(value.ToString("0,0.0", CultureInfo.InvariantCulture));
            Console.WriteLine(String.Format(CultureInfo.InvariantCulture,
                                        "{0:0,0.0}", value));
            // Displays 1,234,567,890.1   

            value = 1234.567890;
            Console.WriteLine(value.ToString("0,0.00", CultureInfo.InvariantCulture));
            Console.WriteLine(String.Format(CultureInfo.InvariantCulture,
                                        "{0:0,0.00}", value));
            // Displays 1,234.57 

            value = 1.2;
            Console.WriteLine(value.ToString("#.##", CultureInfo.InvariantCulture));
            Console.WriteLine(String.Format(CultureInfo.InvariantCulture,
                                            "{0:#.##}", value));
            // Displays 1.2 

            value = 123;
            Console.WriteLine(value.ToString("#####"));
            Console.WriteLine(String.Format("{0:#####}", value));
            // Displays 123 

            value = 123456;
            Console.WriteLine(value.ToString("[##-##-##]"));
            Console.WriteLine(String.Format("{0:[##-##-##]}", value));
            // Displays [12-34-56] 

            value = 1234567890;
            Console.WriteLine(value.ToString("#"));
            Console.WriteLine(String.Format("{0:#}", value));
            // Displays 1234567890

            Console.WriteLine(value.ToString("(###) ###-####"));
            Console.WriteLine(String.Format("{0:(###) ###-####}", value));
            // Displays (123) 456-7890

            value = 1.2;
            Console.WriteLine(value.ToString("0.00", CultureInfo.InvariantCulture));
            Console.WriteLine(String.Format(CultureInfo.InvariantCulture,
                                            "{0:0.00}", value));
            // Displays 1.20

            Console.WriteLine(value.ToString("00.00", CultureInfo.InvariantCulture));
            Console.WriteLine(String.Format(CultureInfo.InvariantCulture,
                                            "{0:00.00}", value));
            // Displays 01.20

            Console.WriteLine(value.ToString("00.00",
                                CultureInfo.CreateSpecificCulture("da-DK")));
            Console.WriteLine(String.Format(CultureInfo.CreateSpecificCulture("da-DK"),
                                "{0:00.00}", value));
            // Displays 01,20 

            value = .086;
            Console.WriteLine(value.ToString("#0.##%", CultureInfo.InvariantCulture));
            Console.WriteLine(String.Format(CultureInfo.InvariantCulture,
                                            "{0:#0.##%}", value));
            // Displays 8.6% 

            value = 86000;
            Console.WriteLine(value.ToString("0.###E+0", CultureInfo.InvariantCulture));
            Console.WriteLine(String.Format(CultureInfo.InvariantCulture,
                                "{0:0.###E+0}", value));
            // Displays 8.6E+4
            value = 1234567890;
            Console.WriteLine(value.ToString("#,,", CultureInfo.InvariantCulture));
            Console.WriteLine(String.Format(CultureInfo.InvariantCulture,
                                            "{0:#,,}", value));
            // Displays 1235   

            Console.WriteLine(value.ToString("#,,,", CultureInfo.InvariantCulture));
            Console.WriteLine(String.Format(CultureInfo.InvariantCulture,
                                            "{0:#,,,}", value));
            // Displays 1  

            Console.WriteLine(value.ToString("#,##0,,", CultureInfo.InvariantCulture));
            Console.WriteLine(String.Format(CultureInfo.InvariantCulture,
                                            "{0:#,##0,,}", value));
            // Displays 1,235
            value = .086;
            Console.WriteLine(value.ToString("#0.##%", CultureInfo.InvariantCulture));
            Console.WriteLine(String.Format(CultureInfo.InvariantCulture,
                                            "{0:#0.##%}", value));
            // Displays 8.6%   
            value = .00354;
            string perMilleFmt = "#0.## " + '\u2030';
            Console.WriteLine(value.ToString(perMilleFmt, CultureInfo.InvariantCulture));
            Console.WriteLine(String.Format(CultureInfo.InvariantCulture,
                                            "{0:" + perMilleFmt + "}", value));
            // Displays 3.54‰
            return value.ToString();
        }

        public static void FormatDate()
        {

            DateTime date1 = new DateTime(2008, 8, 29, 19, 27, 15);
            CultureInfo ci = CultureInfo.InvariantCulture;
            Console.WriteLine("Datum in cultuur van computer {0}.\n", date1.ToString("d, M",
                        CultureInfo.InvariantCulture));
            // Displays 29, 8
            Console.WriteLine(date1.ToString("d MMMM",
                        CultureInfo.CreateSpecificCulture("en-US")));
            // Displays 29 August
            Console.WriteLine(date1.ToString("d MMMM",
                        CultureInfo.CreateSpecificCulture("es-MX")));
            // Displays 29 agosto  
            Console.WriteLine(date1.ToString("dd, MM",
                        CultureInfo.InvariantCulture));
            // 02, 01
            Console.WriteLine(date1.ToString("ddd d MMM",
                        CultureInfo.CreateSpecificCulture("en-US")));
            // Displays Fri 29 Aug
            Console.WriteLine(date1.ToString("ddd d MMM",
                        CultureInfo.CreateSpecificCulture("fr-FR")));
            // Displays ven. 29 août       

            Console.WriteLine(date1.ToString("dddd dd MMMM",
                        CultureInfo.CreateSpecificCulture("en-US")));
            // Displays Friday 29 August
            Console.WriteLine(date1.ToString("dddd dd MMMM",
                        CultureInfo.CreateSpecificCulture("it-IT")));
            // Displays venerdì 29 agosto      
            Console.WriteLine(date1.ToString("hh:mm:ss.f", ci));
            // Displays 07:27:15.0
            Console.WriteLine(date1.ToString("hh:mm:ss.F", ci));
            // Displays 07:27:15
            Console.WriteLine(date1.ToString("hh:mm:ss.ff", ci));
            // Displays 07:27:15.01
            Console.WriteLine(date1.ToString("hh:mm:ss.FF", ci));
            // Displays 07:27:15.01
            Console.WriteLine(date1.ToString("hh:mm:ss.fff", ci));
            // Displays 07:27:15.018
            Console.WriteLine(date1.ToString("hh:mm:ss.FFF", ci));
            // Displays 07:27:15.018
            date1 = new DateTime(70, 08, 04);
            Console.WriteLine(date1.ToString("MM/dd/yyyy g",
                                CultureInfo.InvariantCulture));
            // Displays 08/04/0070 A.D.                        
            Console.WriteLine(date1.ToString("MM/dd/yyyy g",
                                CultureInfo.CreateSpecificCulture("fr-FR")));
            // Displays 08/04/0070 ap. J.-C.

            date1 = new DateTime(2008, 1, 1, 18, 9, 1);
            Console.WriteLine(date1.ToString("h:m:s.F t",
                                CultureInfo.InvariantCulture));
            // Displays 6:9:1 P
            Console.WriteLine(date1.ToString("h:m:s.F t",
                                CultureInfo.CreateSpecificCulture("el-GR")));
            // Displays 6:9:1 µ                        
            date1 = new DateTime(2008, 1, 1, 18, 9, 1, 500);
            Console.WriteLine(date1.ToString("h:m:s.F t",
                                CultureInfo.InvariantCulture));
            // Displays 6:9:1.5 P
            Console.WriteLine("In het Grieks {0}\n", date1.ToString("h:m:s.F t",
                                CultureInfo.CreateSpecificCulture("el-GR")));
            // Displays 6:9:1.5 µ
            date1 = new DateTime(1, 12, 1);
            DateTime date2 = new DateTime(2010, 1, 1);
            Console.WriteLine(date1.ToString("%y"));
            // Displays 1
            Console.WriteLine(date1.ToString("yy"));
            // Displays 01
            Console.WriteLine(date1.ToString("yyy"));
            // Displays 001
            Console.WriteLine(date1.ToString("yyyy"));
            // Displays 0001
            Console.WriteLine(date1.ToString("yyyyy"));
            // Displays 00001
            Console.WriteLine(date2.ToString("%y"));
            // Displays 10
            Console.WriteLine(date2.ToString("yy"));
            // Displays 10
            Console.WriteLine(date2.ToString("yyy"));
            // Displays 2010      
            Console.WriteLine(date2.ToString("yyyy"));
            // Displays 2010      
            Console.WriteLine(date2.ToString("yyyyy"));
            // Displays 02010
        }

        public static void NewCultureInfo()
        {
            decimal kostprijs = 120m;
            string sAnswer = "";

            Console.WriteLine(sAnswer += kostprijs.ToString("C2", new CultureInfo("en-GB")) + "\n");
        }

        public static void Interpolatie()
        {
            string item = "brood";
            decimal amount = 2.25m;
            Console.WriteLine("{0,-10}{1:C}", item, amount);

            Console.WriteLine($"{item}kost {amount}");

            Console.WriteLine($"{nameof(item)}: {item,-10} {nameof(amount)}: {amount:C}");

            string name = "Bob Dylan";
            int age = 80;
            var s = $"{name,20} is {age:D3} year{(age == 1 ? "" : "s")} old.";
            Console.WriteLine(s);
        }

        public static void TekenReeksenSamenvoegen()
        {
            // Drie letterlijke strings die aan elkaar geplakt worden
            string s = "Hello" + " " + "world!";
            Console.WriteLine(s);
        }

        public static void TekenReeksenVergelijken()
        {
            string s = "";
            string sTest = "Eva";

            string color1 = "red";
            string color2 = "green";
            string color3 = "red";

            if (color1 == color3)
            {
                s = "Equal\n";
            }
            if (color1 != color2)
            {
                s = "Not equal\n";
            }

            s = "Resultaat van Equals: " + sTest.Equals("Eva").ToString() + "\n";

            // Enter different values for string1 and string2 to
            // experiement with behavior of CompareTo
            string string1 = "ABC";
            string string2 = "abc";

            int result = string1.CompareTo(string2);

            if (result > 0)
            {
                s = String.Format("{0} is greater than {1}\n", string1, string2);
            }
            else if (result == 0)
            {
                s = String.Format("{0} is equal to {1}\n", string1, string2);
            }
            else if (result < 0)
            {
                s = String.Format("{0} is less than {1}\n", string1, string2);
            }
        }

        public static void CultuurInformatie()
        {
            decimal kostprijs = 30.99m;
            string sAnswer;
            // Vietnamees geldsymbool
            sAnswer = kostprijs.ToString("c", new CultureInfo("vi-VN")) + "\n";
            Console.WriteLine("Vietnamees geld: {0}", sAnswer);
            // UK
            sAnswer = kostprijs.ToString("c", new CultureInfo("en-GB")) + "\n";
            Console.WriteLine("UKgeld: {0}", sAnswer);
        }
    }
}
