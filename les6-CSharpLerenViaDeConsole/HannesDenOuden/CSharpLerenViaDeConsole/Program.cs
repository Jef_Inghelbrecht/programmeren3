﻿using System;

namespace LerenWerkenMetGegevensInCSharp
{
    class Program
    {
        static void Main(string[] args)
        {
            WerkenMetGegevens.CharLerenGebruiken();
            WerkenMetGegevens.ShowAllAsciiValues();
            WerkenMetGegevens.StringsZijnOnveranderlijk();
            WerkenMetGegevens.VerbatimStrings();
            WerkenMetGegevens.FormatNumericSample();
            WerkenMetGegevens.FormatDate();
            WerkenMetGegevens.NewCultureInfo();
            WerkenMetGegevens.Interpolatie();
            WerkenMetGegevens.TekenReeksenSamenvoegen();
            WerkenMetGegevens.TekenReeksenVergelijken();
            WerkenMetGegevens.CultuurInformatie();
            StringBuilderVersusString.StringBuilderVersusStringVoorbeeld();
            StructVersusClass.Uitproberen();
            TabelMetGetallen.TabelTekenen();
        }
    }
}
