﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LerenWerkenMetGegevensInCSharp
{
    class TabelMetGetallen
    {
        public static void TabelTekenen()
        {
            int optelNummer = 0;
            for (int i = 1; i <= 100; i++)
            {
                //Gebruik gemaakt van column-text-formatting via interpolatie
                if (optelNummer == 10)
                {
                    Console.WriteLine();
                    Console.Write($"{i,-5}");
                    optelNummer = 1;
                }
                else
                {
                    Console.Write($"{i,-5}");
                    optelNummer += 1;
                }
            }
        }
    }
}

