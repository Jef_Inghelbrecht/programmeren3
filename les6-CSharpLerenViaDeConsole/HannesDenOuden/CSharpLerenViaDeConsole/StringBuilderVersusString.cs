﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LerenWerkenMetGegevensInCSharp
{
    class StringBuilderVersusString
    {
        // Concatenates to String 
        public static void concat1(String s1)
        {
            // taking a string which 
            // is to be Concatenate 
            String st = "den Ouden";

            // using String.Concat method 
            // you can also replace it with 
            // s1 = s1 + "forgeeks"; 
            s1 = String.Concat(s1, st);
        }

        // Concatenates to StringBuilder 
        public static void concat2(StringBuilder s2)
        { 
            // using Append method 
            // of StringBuilder class 
            s2.Append("den Ouden");
        }

        // Main Method 
        public static void StringBuilderVersusStringVoorbeeld()
        {
            String s1 = "Hannes";
            concat1(s1); // s1 is not changed 
            Console.WriteLine("String klasse: " + s1);

            StringBuilder s2 = new StringBuilder("Hannes");
            concat2(s2); // s2 is changed 
            Console.WriteLine("Stringbuilder klasse: " + s2);
            Console.WriteLine("Je ziet dat er voor het string-voorbeeld nu twee objecten bestaan, string = \"Hannes\" en string=\"den Ouden\". " +
                "Voor het strinbuilder voorbeeld bestaat er slechts één object, namelijk stringbuilder=\"Hannesden Ouden\". Hierdoor is de stringbuilder-klasse" +
                "vele malen efficiënter");
        }
    }
}
